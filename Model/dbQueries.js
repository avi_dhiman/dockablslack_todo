const {bootstrap} = require('./initDB');
const {Logger} = require('../logger');
let dbObj;
bootstrap((db)=> dbObj = db);

module.exports = {
    insertTodo: function (data, collection,cb) {
        dbObj.collection(collection).insertMany(data,cb);
    },
    deleteTodo: function(query, collection,cb){
        dbObj.collection(collection).deleteOne(query, function(err,result){
            cb(result);
        })
    },
    getTodo: function(query,collection,cb){
        dbObj.collection(collection).find(query).sort({creationDate: 1}).toArray(function(err,result){
            console.log(query, result);
            if(err){
                Logger.error(err)
            }
            else
            cb(result);
        });
    }
};
