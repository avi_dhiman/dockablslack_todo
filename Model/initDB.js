const MongoClient =require('mongodb').MongoClient;
const {Logger} = require('../logger');
const {mongo} = require('../configs');
module.exports = {
    bootstrap: (callback) =>{
        MongoClient.connect(mongo.url, function(err,client) {
            Logger.log("connected successfully to the mongo");
            dbObj = client.db(mongo.dbName);
            callback(dbObj);
        });
    }
};
