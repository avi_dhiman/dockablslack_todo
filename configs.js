const path = require('path');

module.exports = {
    slack: {
        token: 'xoxb-858739946161-861409412546-MZkpD5rdlqo5N89fD2bL1co3',
        name: 'Todo-bot',
        channelName: 'todo_task_channel'
    },
    mongo: {
        url: 'mongodb://localhost:27017',
        dbName: 'Todo',
        collectionName: 'TodoDockabl',
    },
    server: {
        port: 3000,
        logsPath: path.resolve(__dirname, './logs')
    }
};
