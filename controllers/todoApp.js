const {slack, mongo} = require('../configs');
const {Logger} = require('../logger');
const SlackBot = require('slackbots');
const shortid = require('shortid');

const {deleteTodo, getTodo, insertTodo} = require('../Model/dbQueries');
const bot = new SlackBot({
    token: slack.token,
    name: slack.name
});
const getLabel = (reqText) => {
    let label = reqText.split('--').find(i => i.startsWith('label='));
    label && (label = label.substring(6));
    return label || '';
};

const getLabelNExpiry = (reqText) => {
    const splicedText = reqText.split('--');
    let label = splicedText.find(i => i.startsWith('label='));
    label && (label = label.substring(6));
    let expiry = splicedText.find(i => i.startsWith('expiry='));
    expiry && (expiry = expiry.substring(7));
    expiry = expiry && new Date(...expiry.split('/').reverse().map((i,index)=>index === 1 ? parseInt(i) - 1 : parseInt(i)));
    return {task: splicedText[0], label: (label || 'green'), expiry: expiry || 'Never' };
};
module.exports = {
    addTodo: (req, res) => {
        let {team_id, team_domain, channel_id, channel_name, user_id, user_name, command, text} = req.body;
        const {task, label, expiry} = getLabelNExpiry(text);
        const dataTobeSaved = {
            taskId: shortid.generate(),
            team_id, team_domain,
            channel_id, channel_name,
            user_id, user_name, command, task, label,
            expiry, creationDate: new Date()
        };

        insertTodo([dataTobeSaved], mongo.collectionName, (err, data) => {
            Logger.log(err, data);
            res.send(bot.postMessageToChannel(slack.channelName, [
                `☆ *${user_name}* created a task:`,
                `*${dataTobeSaved.task}*`,
                `☞ *Task ID:* ${dataTobeSaved.taskId}`,
                `☞ *Created On:* ${dataTobeSaved.creationDate}`,
                `☞ *Label:* ${dataTobeSaved.label}`,
                `☞ *Expiry:* ${dataTobeSaved.expiry}`
            ].join('\n')));
        });
    },
    listTodo: (req, res) => {
        let {team_id, team_domain, channel_id, channel_name, user_id, user_name, text} = req.body;
        let label = getLabel(text);
        const dataTobeSearched = {
            team_id, team_domain, channel_id, channel_name, user_id, user_name,
        };
        if(label) {
            dataTobeSearched.label = label;
        }

        getTodo(dataTobeSearched, mongo.collectionName, (data) => {
            Logger.log(JSON.stringify(data, null, 4));
            const taskStr = '*List of task:* \n\n'+data.map(task=>{
                const expiry = new Date(task.expiry);
                let _isExpired = !isNaN(expiry.getTime()) && expiry < new Date();
                return [
                    `(${user_name}) > *${task.task}*`,
                    `> ☞ *Task ID:* ${task.taskId}`,
                    `> ☞ *${_isExpired ? 'Expired' : 'Expiring on'}* ${ expiry}`,
                    `> ☞ *Label:* ${task.label}`,
                    `> ☞ *Expiry:* ${task.expiry}`
                ].join('\n')
            }).join('\n\n\n\n');
            res.send(bot.postMessageToChannel(slack.channelName, taskStr));
        });
    },
    markTodo: (req, res) => {
        const {text} = req.body;
        deleteTodo({taskId: text.trim()}, mongo.collectionName, data => {
            res.send(bot.postMessageToChannel(slack.channelName, `*Sucessfully deleted task:* ${text}`));
        });
    }
};



