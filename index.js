var express = require('express');
const {Logger} = require('./logger');
var app = express();
const {server} = require('./configs');
const {init} = require('./middleWares/createMiddlewares');
const {initRoutes} = require('./route/bootstrap');

init(app, express);
initRoutes(app);

app.listen(server.port, function () {
    Logger.log("server stared at : ", server.port);
});
