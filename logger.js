const fs = require('fs');
const path = require('path');
const {server} = require('./configs');

function logToFile(type, ...args) {
    const str = ['Logged on: ', new Date().toString(), ' with message: ', ...args].join(' ');
    console[type](str);
    fs.appendFileSync(`${server.logsPath}/message_${type}.txt`, str);
}

if (!fs.existsSync(server.logsPath)){
    fs.mkdirSync(server.logsPath);
}

class Logger {
    static log = (...args) => {
        logToFile('log', ...args);
    };
    static error = (...args) => {
        logToFile('error', ...args);
    };
}

module.exports = {
    Logger
};
