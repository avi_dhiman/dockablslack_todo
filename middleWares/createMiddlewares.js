var bodyParser = require('body-parser');
var path = require('path');

module.exports = {
    init: (app, express) => {
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({ extended: false }));
        const staticPath = path.resolve(__dirname, '../static');
        app.use(express.static(staticPath));
    }
}
