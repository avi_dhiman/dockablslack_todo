const {apiMethods} = require('./routes');

module.exports = {
    initRoutes: (app) =>  {
        Object.entries(apiMethods).forEach(([method, apiList])=> {
            Object.entries(apiList).forEach(([url, callbacks]) => {
                app[method](url, callbacks);
            })
        });
    }
}
