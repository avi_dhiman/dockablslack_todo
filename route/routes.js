const {addTodo, listTodo, markTodo} = require('../controllers/todoApp');
const apiMethods = {
    post: {
        /*
            -default label 'green'
            -default expiry 'Never'
            /addtask ${task_name_string} --label=[green | red | blue | gray] --expiry=dd/mm/yyyy
        */
        '/addtodo': addTodo,
        /*
            -default label ''
            /listtodos --label=[green | red | blue | gray]
        */
        '/listtodos': listTodo,
        '/marktodo': markTodo
    }
};

module.exports = {apiMethods};
